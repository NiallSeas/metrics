resource "aws_cloudwatch_log_metric_filter" "VPC_change" {
  name           = "VPC_change"
  pattern        = "{ ($.eventName = CreateVpc) || ($.eventName = DeleteVpc) || ($.eventName = ModifyVpcAttribute) || ($.eventName = AcceptVpcPeeringConnection) || ($.eventName = CreateVpcPeeringConnection) || ($.eventName = DeleteVpcPeeringConnection) || ($.eventName = RejectVpcPeeringConnection) || ($.eventName = AttachClassicLinkVpc) || ($.eventName = DetachClassicLinkVpc) || ($.eventName = DisableVpcClassicLink) || ($.eventName = EnableVpcClassicLink) }"
  log_group_name = aws_cloudwatch_log_group.VPC.name

  metric_transformation {
    name      = "VPC"
    namespace = "QuodOrbis"
    value     = "1"
  }
}


resource "aws_cloudwatch_log_metric_filter" "RouteTable_change" {
  name           = "RouteTable_change"
  pattern        = "{ ($.eventName = CreateRoute) || ($.eventName = CreateRouteTable) || ($.eventName = ReplaceRoute) || ($.eventName = ReplaceRouteTableAssociation) || ($.eventName = DeleteRouteTable) || ($.eventName = DeleteRoute) || ($.eventName = DisassociateRouteTable) }"
  log_group_name = aws_cloudwatch_log_group.RouteTable.name

  metric_transformation {
    name      = "RouteTable"
    namespace = "QuodOrbis"
    value     = "1"
  }
}


resource "aws_cloudwatch_log_metric_filter" "NetworkGateway_change" {
  name           = "NetworkGateway_change"
  pattern        = "{ ($.eventName = CreateCustomerGateway) || ($.eventName = DeleteCustomerGateway) || ($.eventName = AttachInternetGateway) || ($.eventName = CreateInternetGateway) || ($.eventName = DeleteInternetGateway) || ($.eventName = DetachInternetGateway) }"
  log_group_name = aws_cloudwatch_log_group.NetworkGateway.name

  metric_transformation {
    name      = "NetworkGateway"
    namespace = "QuodOrbis"
    value     = "1"
  }
}



resource "aws_cloudwatch_log_metric_filter" "ACL_change" {
  name           = "ACL_change"
  pattern        = "{ ($.eventName = CreateNetworkAcl) || ($.eventName = CreateNetworkAclEntry) || ($.eventName = DeleteNetworkAcl) || ($.eventName = DeleteNetworkAclEntry) || ($.eventName = ReplaceNetworkAclEntry) || ($.eventName = ReplaceNetworkAclAssociation) }"
  log_group_name = aws_cloudwatch_log_group.ACL.name

  metric_transformation {
    name      = "ACL"
    namespace = "QuodOrbis"
    value     = "1"
  }
}



resource "aws_cloudwatch_log_metric_filter" "Config_change" {
  name           = "Config_change"
  pattern        = "{ ($.eventSource = config.amazonaws.com) && (($.eventName = StopConfigurationRecorder)||($.eventName = DeleteDeliveryChannel)||($.eventName = PutDeliveryChannel)||($.eventName = PutConfigurationRecorder)) }"
  log_group_name = aws_cloudwatch_log_group.Conf.name

  metric_transformation {
    name      = "Conf"
    namespace = "QuodOrbis"
    value     = "1"
  }
}



resource "aws_cloudwatch_log_metric_filter" "Bucket_change" {
  name           = "Bucket_change"
  pattern        = "{ ($.eventSource = s3.amazonaws.com) && (($.eventName = PutBucketAcl) || ($.eventName = PutBucketPolicy) || ($.eventName = PutBucketCors) || ($.eventName = PutBucketLifecycle) || ($.eventName = PutBucketReplication) || ($.eventName = DeleteBucketPolicy) || ($.eventName = DeleteBucketCors) || ($.eventName = DeleteBucketLifecycle) || ($.eventName = DeleteBucketReplication)) }"
  log_group_name = aws_cloudwatch_log_group.Bucket.name

  metric_transformation {
    name      = "Bucket"
    namespace = "QuodOrbis"
    value     = "1"
  }
}



resource "aws_cloudwatch_log_metric_filter" "CMK_change" {
  name           = "CMK_change"
  pattern        = "{ ($.eventSource = kms.amazonaws.com) && (($.eventName = DisableKey) || ($.eventName = ScheduleKeyDeletion)) }"
  log_group_name = aws_cloudwatch_log_group.CMK.name

  metric_transformation {
    name      = "CMK"
    namespace = "QuodOrbis"
    value     = "1"
  }
}



resource "aws_cloudwatch_log_metric_filter" "SignIn_Fail" {
  name           = "SignIn_Fail"
  pattern        = "{ ($.eventName = ConsoleLogin) && ($.errorMessage = "Failed authentication") }"
  log_group_name = aws_cloudwatch_log_group.SignIn.name

  metric_transformation {
    name      = "SignIn"
    namespace = "QuodOrbis"
    value     = "1"
  }
}



resource "aws_cloudwatch_log_metric_filter" "IAM_policy_change" {
  name           = "IAM_policy_change"
  pattern        = "{ ($.eventName = DeleteGroupPolicy) || ($.eventName = DeleteRolePolicy) || ($.eventName = DeleteUserPolicy) || ($.eventName = PutGroupPolicy) || ($.eventName = PutRolePolicy) || ($.eventName = PutUserPolicy) || ($.eventName = CreatePolicy) || ($.eventName = DeletePolicy) || ($.eventName = CreatePolicyVersion) || ($.eventName = DeletePolicyVersion) || ($.eventName = AttachRolePolicy) || ($.eventName = DetachRolePolicy) || ($.eventName = AttachUserPolicy) || ($.eventName = DetachUserPolicy) || ($.eventName = AttachGroupPolicy) || ($.eventName = DetachGroupPolicy) }"
  log_group_name = aws_cloudwatch_log_group.IamPolicy.name

  metric_transformation {
    name      = "IamPolicy"
    namespace = "QuodOrbis"
    value     = "1"
  }
}



resource "aws_cloudwatch_log_metric_filter" "API_call_Unauth" {
  name           = "API_call_Unauth"
  pattern        = "{ ($.errorCode = \"*UnauthorizedOperation\") || ($.errorCode = \"AccessDenied*\") }"
  log_group_name = aws_cloudwatch_log_group.APIcall.name

  metric_transformation {
    name      = "APIcall"
    namespace = "QuodOrbis"
    value     = "1"
  }
}



resource "aws_cloudwatch_log_metric_filter" "MFA_alarm" {
  name           = "MFA_alarm"
  pattern        = "{($.eventName = \"ConsoleLogin\") && ($.additionalEventData.MFAUsed != \"Yes\") && ($.responseElements.ConsoleLogin != \"Failure\") && ($.additionalEventData.SamlProviderArn NOT EXISTS) }"
  log_group_name = aws_cloudwatch_log_group.MFA.name

  metric_transformation {
    name      = "MFA"
    namespace = "QuodOrbis"
    value     = "1"
  }
}



resource "aws_cloudwatch_log_metric_filter" "Cloudtrail_change" {
  name           = "Cloudtrail_change"
  pattern        = "{ ($.eventName = CreateTrail) || ($.eventName = UpdateTrail) || ($.eventName = DeleteTrail) || ($.eventName = StartLogging) || ($.eventName = StopLogging) }"
  log_group_name = aws_cloudwatch_log_group.Cloudtrail.name

  metric_transformation {
    name      = "Cloudtrail"
    namespace = "QuodOrbis"
    value     = "1"
  }
}



resource "aws_cloudwatch_log_metric_filter" "SecurityGroup_change" {
  name           = "SecurityGroup_change"
  pattern        = "{ ($.eventName = AuthorizeSecurityGroupIngress) || ($.eventName = AuthorizeSecurityGroupEgress) || ($.eventName = RevokeSecurityGroupIngress) || ($.eventName = RevokeSecurityGroupEgress) || ($.eventName = CreateSecurityGroup) || ($.eventName = DeleteSecurityGroup) }"
  log_group_name = aws_cloudwatch_log_group.SecurityGroup.name

  metric_transformation {
    name      = "SecurityGroup"
    namespace = "QuodOrbis"
    value     = "1"
  }
}

resource "aws_cloudwatch_log_metric_filter" "RootAccount_Use" {
  name           = "RootAccount_Use"
  pattern        = "{ $.userIdentity.type = "Root" && $.userIdentity.invokedBy NOT EXISTS && $.eventType != "AwsServiceEvent" }"
  log_group_name = aws_cloudwatch_log_group.Root.name

  metric_transformation {
    name      = "Root"
    namespace = "QuodOrbis"
    value     = "1"
  }
}
